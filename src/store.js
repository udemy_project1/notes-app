import { configureStore } from "@reduxjs/toolkit";
import notes from "./features/notes";

const store = configureStore({
	reducer: {
		notes,
	},
});

export default store;
