import NotesList from "./components/NotesList";
import { useSelector, useDispatch } from "react-redux";
import { getNotesFromAPI } from "./features/notes";
import Sidebar from "./components/Sidebar";
import SideNotes from "./components/SideNotes";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import DisplayedNote from "./components/DisplayedNote";
import Edit from "./components/Edit";

function App() {
	const notesValues = useSelector((state) => state.notes);
	const dispatch = useDispatch();

	if (!notesValues.list) {
		dispatch(getNotesFromAPI());
	}

	return (
		<div className="bg-slate-800 min-h-screen flex">
			<BrowserRouter>
				<Sidebar />
				<SideNotes />
				<Routes>
					<Route
						path="/"
						element={<NotesList />}
					/>
					<Route
						path="/note/:id"
						element={<DisplayedNote />}
					/>
					<Route
						path="/edit"
						element={<Edit />}
					/>
					<Route
						path="/edit/:id"
						element={<Edit />}
					/>
				</Routes>
			</BrowserRouter>
		</div>
	);
}

export default App;
