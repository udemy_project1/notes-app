import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { Link, useNavigate, useParams } from "react-router-dom";
import { deleteNote } from "../features/notes";

export default function DisplayedNote() {
	const notes = useSelector((state) => state.notes);
	const { id } = useParams();
	const navigate = useNavigate();
	const actualNote = notes?.list?.find((note) => note.id === id);
	const dispatch = useDispatch();
	return (
		<div className="p-10">
			<Link
				to="/"
				className="bg-slate-300 text-slate-800 text-md rounded px-2 py-1 mr-2">
				Notes
			</Link>
			<Link
				to={`/edit/${id}`}
				className="bg-green-600 text-slate-200 text-md rounded px-2 py-1 mr-2">
				Mettre à jour
			</Link>
			<button
				onClick={() => {
					dispatch(deleteNote(id));
					navigate("/");
				}}
				className="bg-red-600 text-slate-200 text-md rounded px-2 py-1">
				Supprimer
			</button>
			<p className="text-slate-100 text-4xl mb-2 mt-8">{actualNote?.title}</p>
			<p className="text-slate-200 text-xl mb-4">{actualNote?.subtitle}</p>
			<p className="text-slate-300 ">{actualNote?.bodyText}</p>
		</div>
	);
}
