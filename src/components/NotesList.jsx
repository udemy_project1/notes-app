import React from "react";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";

export default function NotesList() {
	const notes = useSelector((state) => state.notes);
	return (
		<div className="p-10 w-full ">
			<p className="text-xl text-slate-200 mb-6">Bienvenue sur Notes101</p>
			<ul className="grid lg:grid-cols-2 xl:grid-cols-4 2xl:grid-cols-5 gap-6">
				{notes.list &&
					notes.list.map((note) => (
						<li
							className="bg-slate-100 hover:bg-slate-50 rounded cursor-pointer"
							key={note.id}>
							<Link
								className="block p-4 w-full"
								to={`/edit/${note.id}`}>
								<span className="block text-lg font-semibold">{note.title}</span>
								<span className="block text-gray-700">{note.subtitle}</span>
							</Link>
						</li>
					))}
			</ul>
		</div>
	);
}
