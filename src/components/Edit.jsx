import { nanoid } from "nanoid";
import React from "react";
import { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { addNoteFromUser } from "../features/notes";
import { useParams } from "react-router-dom";
import { editNoteFromUser } from "../features/notes";

export default function Edit() {
	const dispatch = useDispatch();
	const { id } = useParams();
	const notes = useSelector((state) => state.notes);

	const [inputsState, setInputsState] = useState({
		title: "",
		subtitle: "",
		bodyText: "",
	});
	const [showValidation, setShowValidation] = useState({
		title: false,
		subtitle: false,
		bodyText: false,
	});
	const handleSubmit = (e) => {
		e.preventDefault();
		if (Object.values(inputsState).every((value) => value)) {
			setShowValidation({
				title: false,
				subtitle: false,
				bodyText: false,
			});
			if (id && notes.list) {
				dispatch(editNoteFromUser({ ...inputsState, id }));
			} else {
				dispatch(addNoteFromUser({ ...inputsState, id: nanoid(8) }));
				setInputsState({
					title: "",
					subtitle: "",
					bodyText: "",
				});
			}
		} else {
			for (const [key, value] of Object.entries(inputsState)) {
				if (value.length === 0) {
					setShowValidation((state) => ({ ...state, [key]: true }));
				} else {
					setShowValidation((state) => ({ ...state, [key]: false }));
				}
			}
		}
	};
	useEffect(() => {
		if (id && notes.list) {
			const actualNote = notes.list.find((note) => note.id === id);
			setInputsState({
				title: actualNote.title,
				subtitle: actualNote.subtitle,
				bodyText: actualNote.bodyText,
			});
		} else {
			setInputsState({
				title: "",
				subtitle: "",
				bodyText: "",
			});
		}
	}, [notes, id]);
	return (
		<div className="w-full p-10">
			<p className="text-slate-100 text-xl mb-4">Ajouter une note</p>
			<form onSubmit={handleSubmit}>
				<label
					htmlFor="title"
					className="mb-2 block text-slate-100">
					Le titre
				</label>
				<input
					onChange={(e) => setInputsState({ ...inputsState, title: e.target.value })}
					type="text"
					value={inputsState.title}
					className="p-2 text-md block w-full rounded bg-slate-200"
					id="title"
					spellCheck={false}
				/>
				{showValidation.title && <p className="text-red-400 mb-2">Veuillez renseigner un titre</p>}
				<label
					htmlFor="subtitle"
					className="mb-2 block text-slate-100 mt-4">
					Le sous titre
				</label>
				<input
					onChange={(e) => setInputsState({ ...inputsState, subtitle: e.target.value })}
					type="text"
					value={inputsState.subtitle}
					className="p-2 text-md block w-full rounded bg-slate-200"
					id="subtitle"
					spellCheck={false}
				/>
				{showValidation.subtitle && <p className="text-red-400 mb-2">Veuillez renseigner sous titre</p>}
				<label
					htmlFor="bodyText"
					className="mb-2 mt-4 block text-slate-100">
					Le contenu de la note
				</label>
				<textarea
					id="bodyText"
					spellCheck={false}
					value={inputsState.bodyText}
					className="w-full min-h-[300px] bg-slate-200 p-2 rounded"
					onChange={(e) => setInputsState({ ...inputsState, bodyText: e.target.value })}></textarea>
				{showValidation.bodyText && <p className="text-red-400 mb-2">Veuillez ecrire du contenu</p>}
				<button className="mt-4 px-3 py-1 bg-slate-100 rounded">Enregistrer</button>
			</form>
		</div>
	);
}
